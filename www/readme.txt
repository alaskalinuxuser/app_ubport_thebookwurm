ReadMe file for distribution of - -
                       ''The Book''
               with notes and resources from 
                      thebookwurm.com

COPYRIGHT NOTICE: (Copyright, David Wurm, 1999 - 2021)

All rights to the lessons and notes contained in this publication 
are reserved by the editor/author.
 - You may use, copy and distribute these materials on paper or by 
   electronic means, in their original form (without further editing), 
   in whole or in part, as long as you do so on a not-for-profit basis. 
 - You may not distribute or transmit this material by any means for 
   financial profit. 
 - You may not receive financial reimbursement for any distributed 
   materials, except for actual cost of paper or media. (This policy is 
   established according to Isaiah 55:1-3, and Matthew 10:8.)
 - You may not alter this material in any way without written permission 
   from the editor/author. Please contact the editor (see contact 
   information at end) with any suggestions or corrections. We recommend 
   that you visit our website periodically to ensure that you are using 
   the most recent file revisions.
 - You may not insert your own contact information within this publication 
   without written permission from the editor.

NOTE: Materials written by other authors are included by the copyright 
holders' express permission or are in the public domain due to their age. 
Inclusion of such copyrighted materials within "'The Book' from thebookwurm.com", 
does not convey license for distribution of these materials in any other context, 
neither does it void the rights of the copyright holders. Where applicable, 
contact information is provided for authors and publishers.

   These studies have been produced with the desire that the Bible be 
clearly understood by as many people as possible. You may freely use, 
print, and distribute these materials, as long as you agree to the few 
limitations in the paragraphs above.

DISTRIBUTION FILES -

The Zip file (the_book.zip), which you have now decompressed into a 
new directory (default name: /TheBook/ ), contains:
     Two files:
          readme.txt    (The file that you are reading.)
          go_book.htm   (A program start file. See below.)
     One directory, which contains more than 2000 files.
          /the_book     (This directory contains the entire website program,
                         including another start file. See below.)
          
     This sub-directory contains all files for program operation. 
     To facilitate electronic distribution (eg., from other websites) 
     all files for "The Book" are contained in one large directory 
     (approx. 22 MB). The entire program is written in HTML and is 
     compatible with most internet browsers. 
     
     The 'start file' within this directory is: go_bible.htm
     The two start files (one in the root directory, and this one in
     the program directory) are different. You should 'bookmark' the
     start file that you choose, to facilitate opening the program in 
     your browser, on future occasions.
     
     For posting 'the Book' in a single directory (as required by 
     some website hosts), you will need to use the start file within
     the program file directory (go_bible.htm).
     
     Of course, as long as my site is functional, you may add a link
     from your site to mine. Sample link: 
     <a href="http://www.thebookwurm.com/go_bible.htm">Link to The Book</a> 
     
     You may also share the_book.zip (9.2 MB) directly, via USB media, by
     email, etc. 
   
    Note 1.: The two start files are internally different and cannot be 
       interchanged. Observe that their names also differ.

    Note 2.: All file names are in lower case letters. This is important
       because some operating systems (like Linux) are case sensitive. 
       Many web host services use Linux based servers. To ensure proper
       operation, be sure to keep file names in lower case only.
       (For example: go_bible.htm must not be changed to Go_Bible.htm)

CONTACT INFORMATION
  Editor:      David Wurm
  Produced by: The Book Wurm
               PO Box 60207
               Fairbanks, AK  99706
  Phone/FAX:   907-474-8980
  Website:     www.theBookwurm.com
  E-mail:      david@theBookwurm.com
