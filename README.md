# TheBookWurm

Looking for a Bible and reference material on your Ubuntu Touch / UBports phone? Tired of using up all of your data with Internet usage to read and study the Bible?

This app is just that. With the entire "TheBookWurm.com" website (used by permission, please see the license files) available in one click from the OpenStore. With two different multi-frame views, and the ability to swipe-to-scroll, pinch-to-zoom, you can study God's word off-line!

Included is the complete KJV Bible, as well as numerous reference materials, notes, and book sketches by the author of the website, Dave Wurm. A great compilation of material that will help you in your day to day walk.


![ScreenShot](https://gitlab.com/alaskalinuxuser/app_ubport_thebookwurm/-/blob/master/ss1.png)
![ScreenShot](https://gitlab.com/alaskalinuxuser/app_ubport_thebookwurm/-/blob/master/ss2.png)
![ScreenShot](https://gitlab.com/alaskalinuxuser/app_ubport_thebookwurm/-/blob/master/ss3.png)
